<?php get_header(); // div#main is opened in header.php ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <article>
    <h2 class="page-title"><?php the_title(); ?></h2>
    <?php the_content(); ?>
    <?php edit_post_link('Edit This Page', '<p class="edit-link">', '</p>'); ?>
  </article>
<?php endwhile; else: ?>
  <h2 class="page-title">Not Found</h2>
  <p>Sorry, the item you requested could not be found.</p>
<?php endif; ?>

<?php get_footer(); // div#main is closed in footer.php ?>