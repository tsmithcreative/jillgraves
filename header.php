<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width" />

    <title><?php echo tsmith_make_title(); ?></title>

    <?php // Include the Google Webfonts Stylesheet for 'Raleway' ?>
    <link href='http://fonts.googleapis.com/css?family=Raleway:200,700,400' rel='stylesheet' type='text/css'>
    <?php // Include the html5shiv for old versions of Internet Explorer ?>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script>window.html5 || document.write('<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/html5shiv.js"><\/script>')</script>
    <![endif]-->
    <?php wp_head(); ?>
  </head>
  <body>
    <div id="container">
      <header>
        <a href="<?php bloginfo('url')?>">
          <h1 id="headline">Jill Graves Portfolio</h1>
          <div id="logo">&nbsp;</div>
        </a>
      </header>
      <nav>
        <ul>
          <?php wp_list_pages('title_li=&echo=1&depth=-1'); ?>
        </ul>
      </nav>
      <div id="main">
