<?php get_header(); // div#main is opened in header.php ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <article>
    <h2 class="page-title"><?php the_title(); ?></h2>
    <?php the_content(); ?>
    <?php edit_post_link('Edit This Post', '<p class="edit-link">', '</p>'); ?>
    <div class="entry-meta">
      <div class="post-date"><?php the_date(); ?></div>

      <?php $categories_list = get_the_category_list( ', ' ); ?>
      <?php if ( $categories_list ): ?>
        <div class="cat-links"> Posted in: <?php echo $categories_list; ?> </div>
      <?php endif; // End if categories ?>

      <?php $tags_list = get_the_tag_list( '', ', ' ); ?>
      <?php if ( $tags_list ): ?>
        <div class="tag-links"> Tagged: <?php echo $tags_list; ?> </div>
      <?php endif; // End if $tags_list ?>

    </div><!-- #entry-meta -->
  </article>
<?php endwhile; else: ?>
  <h2 class="page-title">Not Found</h2>
  <p>Sorry, the item you requested could not be found.</p>
<?php endif; ?>

<?php get_footer(); // div#main is closed in footer.php ?>