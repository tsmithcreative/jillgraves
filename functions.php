<?php

/**
 * Inlcude tsmith512's WordPress utility and cleaning functions
 */
include ('functions-tsmith.php');

/**
 * jillgraves_enqueue_scripts() runs on wp_enqueue_scripts to add
 * theme-specific CSS and JS. Bail if we're in admin.
 */
function jillgraves_enqueue_scripts() {
  if ( is_admin() ) return;

  // The primary stylesheet
  $jillgraves_styles = (get_stylesheet_directory_uri() . '/css/main.css');
  wp_enqueue_style('jillgraves_styles', $jillgraves_styles);

  // The colorbox stylesheet
  $jillgraves_cb_styles = (get_stylesheet_directory_uri() . '/css/colorbox.css');
  wp_enqueue_style('jillgraves_cb_styles', $jillgraves_cb_styles);

  // Modernizr: load in head to prevent the flash of unstyled content
  $jillgraves_modernizr = (get_stylesheet_directory_uri() . '/js/modernizr-latest.dev.js');
  wp_enqueue_script('jillgraves_modernizr', $jillgraves_modernizr, null, null, false );

  // Colorbox: for gallery images
  $jillgraves_colorbox = (get_stylesheet_directory_uri() . '/js/jquery.colorbox-min.js');
  wp_enqueue_script('jillgraves_colorbox', $jillgraves_colorbox, array( 'jquery' ), null, true );

  // The theme-specific javascript
  $jillgraves_scripts = (get_stylesheet_directory_uri() . '/js/main.js');
  wp_enqueue_script('jillgraves_scripts', $jillgraves_scripts, array( 'jquery' ), null, true );
}
add_action('wp_enqueue_scripts','jillgraves_enqueue_scripts');
