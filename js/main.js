(function($){
  $(document).ready(function(){
    /* Use Colorbox to open gallery images */
    $('div.gallery.link-file a').colorbox({
      maxWidth: "90%",
      maxHeight: "90%",
      fixed: true,
      opacity: 0.85,
    })
  });
})(jQuery);