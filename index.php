<?php get_header(); // div#main is opened in header.php ?>

<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <article>
      <a href="<?php the_permalink(); ?>"><h2 class="page-title"><?php the_title(); ?></h2></a>
      <?php the_excerpt(); ?>
      <a class="readmore-link" href="<?php the_permalink(); ?>">Read More &rarr;</a>
    </article>
  <?php endwhile; ?>
  <div class="posts-nav">
    <?php next_posts_link("&larr; Older posts"); ?>&emsp;<?php previous_posts_link("Newer posts &rarr;"); ?>
  </div><!--/.posts-nav-->
<?php else: ?>
  <h2 class="page-title">Not Found</h2>
  <p>Sorry, the item you requested could not be found.</p>
<?php endif; ?>

<?php get_footer(); // div#main is closed in footer.php ?>